package com.hendisantika.springbootneo4jexample.controller;

import com.hendisantika.springbootneo4jexample.entity.User;
import com.hendisantika.springbootneo4jexample.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-neo4j-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/08/18
 * Time: 07.19
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/rest/neo4j/user")
public class UserController {
    @Autowired
    UserService userService;


    @GetMapping
    public Collection<User> getAll() {
        return userService.getAll();
    }
}
