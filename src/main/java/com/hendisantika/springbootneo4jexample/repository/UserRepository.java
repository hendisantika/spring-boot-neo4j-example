package com.hendisantika.springbootneo4jexample.repository;

import com.hendisantika.springbootneo4jexample.entity.User;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-neo4j-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/08/18
 * Time: 07.17
 * To change this template use File | Settings | File Templates.
 */
public interface UserRepository extends Neo4jRepository<User, Long> {

    @Query("MATCH (u:User)<-[r:RATED]-(m:Movie) RETURN u,r,m")
    Collection<User> getAllUsers();
}