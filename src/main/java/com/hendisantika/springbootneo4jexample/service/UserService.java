package com.hendisantika.springbootneo4jexample.service;

import com.hendisantika.springbootneo4jexample.entity.User;
import com.hendisantika.springbootneo4jexample.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-neo4j-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/08/18
 * Time: 07.18
 * To change this template use File | Settings | File Templates.
 */
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public Collection<User> getAll() {
        return userRepository.getAllUsers();
    }
}