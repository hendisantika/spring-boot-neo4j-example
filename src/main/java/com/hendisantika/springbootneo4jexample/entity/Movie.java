package com.hendisantika.springbootneo4jexample.entity;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-neo4j-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/08/18
 * Time: 07.15
 * To change this template use File | Settings | File Templates.
 */
@NodeEntity
public class Movie {

    @GraphId
    private Long id;
    private String title;
    private String director;

    public Movie() {
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDirector() {
        return director;
    }
}